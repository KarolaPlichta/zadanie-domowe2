
public class CartPosition {
	
	
	private double quantity;
	private Product product;
	
	
	
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public String getProductName() {
		return product.getName();
	}
	
	public double getProductPrize() {
		return Product.getPrize();
	}



	
	

}
