import java.util.ArrayList;
	
public class Cart {
	

		private ArrayList<CartPosition> positions;
		private ArrayList<Discount> discounts;
		private String clientStatus;
		private double totalPrice;
		
		public Cart(String status){
			this.setClient(status);
			this.positions = new ArrayList<CartPosition>();
			this.discounts = new ArrayList<Discount>();
			
		}
		
		public void addPosition(CartPosition s){
			this.positions.add(s);
		}
		public ArrayList<CartPosition> getPosition(){
			return this.positions;
		}
		
		public void addDiscount(Discount s){
			this.discounts.add(s);
		}
		
		public ArrayList<Discount> getDiscounts(){
			return this.discounts;
		}
		
		
		public String getClient() {
			return clientStatus;
		}

		public void setClient(String clientStatus) {
			this.clientStatus = clientStatus;
		}
		
		public double countDiscount(){
			this.totalPrice = 0;
			
			for(CartPosition current: this.positions){
				
				this.totalPrice += (current.getQuantity() * current.getProductPrize());
				for(Discount discount: this.discounts){
				  
						totalPrice -= (current.getQuantity() * current.getProductPrize() * discount.discount());
			}
			
		}
			return totalPrice;
		}

	



		

	}

