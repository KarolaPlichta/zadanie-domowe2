
public class Product {
	private String name;
	private String description;
	private static double prize;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public static double getPrize() {
		return prize;
	}
	public void setPrice(double prize) {
		Product.prize = prize;
	}

}
